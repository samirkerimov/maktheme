# database structure 

## Home ##

* Home_id
* Home_link
* Home_title
* Home_heading
* Home_slide
* Slide_on_of

## Services ##

* Servıces_id
* Services_link
* Services_title
* Services_heading
* Services_detail
* Services_item

## work ##

* Work_id
* Work_link
* Work_title
* Work_heading
* Work_detail
* Work_item

## about ##

* About_id
* About_link
* About_title
* About_heading
* About_detail
* About_cat
* About_cat_item

## clients ##

* Clients_id
* Client_link
* Clients_title
* Clients_heading
* Client_img
* Client_img_detail
* Client_portfolio
* Client_count

##  team ##

* Team_id
* Team_link
* Team_title
* Team_detail
* Team_item
* Team_item_detail

## skills ##

* Skills_id
* Skills_link
* Skills_title
* Skills_detail
* Skill

## menu ##

* Menu_id
* Menu_title
* Menu_level

## links ##

* Links_id
* Links_name
* Link_url

## contact ##

* Contact_id
* Contact_title
* Contact_heading
* Contact_detail
* Contact_form
